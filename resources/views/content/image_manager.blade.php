@extends('layouts.app')

@section('content')

<div class = "row ">
    
    <div class = "col-xs-3 left-block" >
           <div class = "row"> 
        <div class = "col-xs-12 block">
            
            <ul>
            <li><a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-database fa-stack-1x"></i>
                </span>
               cms</a> 
            </li>
                <li>
                <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-folder-open fa-stack-1x"></i>
                </span>
                    content</a>
            
                
                
                </li>
                <li>
               <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-file fa-stack-1x"></i>
                </span>
                layout
                    </a>
                
                </li>
                </ul>
            </div></div>
        
        </div>
    
     
    
<div class="container col-xs-8" id="content">
    <div class="row section-header">
        <div class="col-xs-12">
            <h1>Content - Image Manager</h1>
            <div><hr class="line"></div>
        </div>
    </div>

    <div class="row section-content">
      <div class="col-xs-12">

        <h3>Upload Image </h3>
      <div class ="form1">
    <input type="text" name="input" value ="  File name">
    <a href="#" class="btn btn-primary" onclick="document.getElementById('fileID').click(); return false;" >Browse</a>
    <a href="#" class="btn btn-primary">Upload</a>
          </div>
          
         
          
        

        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Images</th>
                <th>Active</th>
                <th>Page</th>
                <th>Edit/Delete</th>
              </tr>
            </thead>
            <tbody>

              
                  <tr>
                    <td>abc</td>
                    <td><ul><li class ="color-li-green"><a href="{{ url('#') }}"><span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check  fa-inverse fa-stack-1x"></i>
                        </span></a></li></ul></td>
                    <td>10/10/99</td>

                    {{-- Edit/Delete links --}}
                    <td>
                      <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul>

                    </td>

                  </tr>
                
                  <tr>
                    <td>qwqwqw</td>
                    <td><ul><li ><a href="{{ url('#') }}"><span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-times  fa-inverse fa-stack-1x"></i>
                        </span></a></li></ul></td>
                    <td>10/10/99</td>

                    {{-- Edit/Delete links --}}
                    <td>
                      <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul>

                    </td>

                  </tr>
          
            </tbody>
          </table>

      </div>
    </div>
    </div>

    </div>

@endsection
