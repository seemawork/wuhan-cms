@extends('layouts.app')

@section('content')

<div class = "row ">
    
    <div class = "col-xs-3 left-block" >
           <div class = "row"> 
        <div class = "col-xs-12 block">
            
            <ul>
            <li><a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-database fa-stack-1x"></i>
                </span>
               cms</a> 
            </li>
                <li>
                <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-folder-open fa-stack-1x"></i>
                </span>
                    content</a>
            
                
                
                </li>
                <li>
               <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-file fa-stack-1x"></i>
                </span>
                layout
                    </a>
                
                </li>
                </ul>
            </div></div>
        
        </div>
    
     
    
<div class="container col-xs-8 layout_newlink" id="content">
    <div class="row section-header">
        <div class="col-xs-12">
            <h1>Content - File Manager - Edit File  </h1>
            <div><hr class="line"></div>
        </div>
    </div>
   
     <div class="row section-content">
      <div class="col-xs-12">

        
            <!-- Login Form -->
            
        <div class="form-group img-width ">
            <img class ="" src="{{URL::asset('/images/word.png')}}" alt="Smiley face" >
          </div>
           <div class="form-group">
                    <div class="col-xs-4  color-li-primary trash ">
                        <a href="#"><span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash-o   fa-inverse fa-stack-1x"></i>
                        </span></a>
                       

                        
                    </div>
                </div>
          
          <div class = "col-xs-12">
              
         <form class="form-horizontal" >

                
                <div class="form-group">
                    <div class="col-xs-4 text-area ">
                        <input id="name" type="name" class="form-control" name="email" value="" placeholder="" autofocus="">

                                            </div>
                </div>

                
                <div class="form-group">
                    <div class="col-xs-4 text-area">
                        <input id="password" type="text" class="form-control" name="password" value="" placeholder="" autofocus="">

                                            </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-4  green-tick color-li-green">
                        <a href="{{ url('#') }}"><span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check  fa-inverse fa-stack-1x"></i>
                        </span></a>
                       

                        
                    </div>
                </div>
               
                
             
                <div class="form-group">
                    <div class="col-xs-8 btn-padding">
                        
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>

                       
                    </div>
                </div>
            </form>
              </div>
          
         
        </div>
    </div>
    
    
    
    
    
    </div>    <!--right side column finish-->
    </div>  <!--row finish-->
    

@endsection
