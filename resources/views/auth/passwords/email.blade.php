@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container" id="content">
    <div class="row section-header">
        <div class="col-xs-12">
            <h1>Reset Password</h1>
            <div><hr class="line"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <!-- Reset Password Form -->
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}

                <!-- Email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-4">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Login / Forgot Password -->
                <div class="form-group">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary">
                            Send Password Reset Link
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
