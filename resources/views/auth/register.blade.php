@extends('layouts.app')

@section('content')
<div class="container" id="content">
    <div class="row section-header">
        <div class="col-xs-12">
            <h1>Register</h1>
            <div><hr class="line"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <!-- Register Form -->
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}

                <!-- Name -->
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="col-xs-4">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="name" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-4">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Password -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-4">
                        <input id="password" type="password" class="form-control" name="password" placeholder="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Password Confirm -->
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <div class="col-xs-4">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="confirm password" required>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Register -->
                <div class="form-group">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
