@extends('layouts.app')

@section('content')
<div class="container" id="content">
    <div class="row section-header">
        <div class="col-xs-12">
            <h1>{{ trans('messages.login') }}</h1>
            <div><hr class="line"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <!-- Login Form -->
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <!-- Email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-4">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email"  autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Password -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-4">
                        <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="password"  autofocus>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Remember Me -->
                <div class="form-group">
                    <div class="col-xs-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                </div>

                <!-- Login / Forgot Password -->
                <div class="form-group">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>

                        <a class="btn btn-link" href="{{ url('/password/reset') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
