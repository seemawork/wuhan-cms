@extends('layouts.app')

@section('content')
<div class="container" id="content" >
    <div class="row section-header">
        <div class="col-xs-12">
            <h1>Home</h1>
            <div><hr class="line"></div>
        </div>
    </div>

    <div class="row">
        <!-- CMS -->
        <div class="col-xs-4 dashboard-block">
          <div class="block">
            <h3>
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-database fa-stack-1x"></i>
                </span>
                cms
            </h3>

            <p>
                This is where you can preview your website
            </p>

            <ul>
                <li><a href="{{ url('#') }}">Home</a></li>
                <li><a href="{{ url('#') }}">View Site</a></li>
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
            </ul>
        </div>
        </div>

        <!-- Content -->
        <div class="col-xs-4 dashboard-block">
            <div class="block">
            <h3>
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-folder-open fa-stack-1x"></i>
                </span>
                content
            </h3>

            <p>
                This is where you can edit the content
            </p>

            <ul>
                <li><a href="{{ url('/pages') }}">Pages</a></li>
                <li><a href="{{ url('/image') }}">Image Manager</a></li>
                <li><a href="{{ url('/file') }}">File Manager</a></li>
            </ul>
          </div>
        </div>

        <!-- Layout -->
        <div class="col-xs-4 dashboard-block">
          <div class="block">

            <h3>
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-file fa-stack-1x"></i>
                </span>
                layout
            </h3>

            <p>
                This is where you can layout the look and feel of the website
            </p>

            <ul>
                <li><a href="{{ url('#') }}">Menu Manager</a></li>
                <li><a href="{{ url('#') }}">Site Admin</a></li>
            </ul>
        </div>
        </div>


    </div>
</div>
@endsection
