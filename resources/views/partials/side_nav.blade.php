<div class="col-xs-3 left-block">
    <div class="row">
        <div class="col-xs-12 block">
            <ul>
                <li>
                    <a href="#">
                        <span class="fa-stack icon">
                          <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                          <i class="fa fa-database fa-stack-1x"></i>
                      </span> cms
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa-stack icon">
                            <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                            <i class="fa fa-folder-open fa-stack-1x"></i>
                        </span> content
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa-stack icon">
                          <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                          <i class="fa fa-file fa-stack-1x"></i>
                      </span> layout
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
