<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    @yield('head')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>
    <header id="header">
      <div>
        <div class="container">
          <div class="col-xs-12">
            <img id="main-logo" src="{{URL::asset('/images/logo.png')}}" alt="Wuhan Xingye Company Logo">
          </div>
        </div>
      </div>

      <nav class="navbar navbar-default navbar-top">
        <div class="container">
          <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
              <span class="sr-only">Toggle Navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
              <span class="fa-stack icon">
                <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                <i class="fa fa-user fa-stack-1x"></i>
              </span>

              {{-- User is not logged in so display app name --}}
              @if (Auth::guest())
              {{ config('app.name', 'Laravel') }}

              {{-- Display Username --}}
              @else
              Welcome: {{ Auth::user()->name }}
              @endif
            </a>
          </div>

          <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
              &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

            <!-- Locale Switcher -->
            <li class="dropdown locale-switcher">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    {{ Config::get('languages')[App::getLocale()] }}
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    @foreach (Config::get('languages') as $lang => $language)
                        @if ($lang != App::getLocale())
                            <li>
                                <a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </li>

              <!-- Help -->
              <li>
                <a href="{{ url('/faq') }}">
                  <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-question fa-stack-1x"></i>
                  </span>
                </a>
              </li>

              <!-- Settings -->
              <li>
                <a href="{{ url('/settings') }}">
                  <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-cog fa-stack-1x"></i>
                  </span>
                </a>
              </li>

              <!-- Search -->
              <li>
                <a href="{{ url('#') }}">
                  <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-search fa-stack-1x"></i>
                  </span>
                </a>
              </li>

              <!-- Logout -->
              <li>
                <a href="{{ url('/logout') }}"   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-sign-out fa-stack-1x"></i>
                  </span>
                </a>

                <!-- Logout Form -->
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    @yield('content')

    <!-- Footer START -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="footer-nav">
                        <li><a href="{{ url('#') }}">Home</a> </li>
                        <li><a href="{{ url('#') }}">View Site</a> </li>
                        <li><a href="{{ url('#') }}">Pages</a> </li>
                        <li><a href="{{ url('#') }}">Image Manager</a> </li>
                        <li><a href="{{ url('#') }}">File Manager</a> </li>
                        <li><a href="{{ url('#') }}">Menu Manager</a> </li>
                        <li><a href="{{ url('#') }}">Site Admin</a> </li>
                    </ul>
                    <p>&copy; Copyright Wuhan Xingye 2016</p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </footer>
    <!-- Footer END -->

    <!-- Scripts -->
    <script src="/js/app.js"></script>

    @yield('scripts')
</body>
</html>
