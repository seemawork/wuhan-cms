@extends('layouts.app')

@section('head')
    <!-- TinyMCE -->
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker" ).datepicker();

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#image_preview').prop('src', e.target.result).show();
                    }

                    reader.readAsDataURL(input.files[0]);
                    $('#image_name').text(input.files[0].name);
                }
            }

            // Update Image preview when a new image is used
            $("#uploaded_file").change(function () {
                readURL(this);
                $('#image_preview').show();
            });

            // Remove image from preview when browse is clicked
            $("#uploaded_file").click(function () {
                $('#image_preview').attr('src','');
            });

            // Remove the uploaded image
            $('#cancel_upload').click(function(e) {
               $('#uploaded_file').val("");
               $('#image_name').text("");
               $('#image_preview').attr("src","");
            });
        });
    </script>
@endsection

@section('content')

<div class="row ">

    @include('partials.side_nav')

    <div class="container col-xs-8 layout_newlink" id="content">
        <div class="row section-header">
            <div class="col-xs-12">
                <h1>Layout - Site Manager - New Article </h1>
                <div>
                    <hr class="line">
                </div>
            </div>
        </div>

        <div class="row section-content">
            <div class="col-xs-12">

                <h3>Article Properties </h3>

                <!-- Create Article Form -->
                <form id="form" class="form-horizontal" action="{{ url('/articles/create') }}" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <!-- Title, Date and Author  -->
                    <div class="form-group">

                        <div class="col-xs-4 {{ $errors->has('title') ? ' has-error' : '' }}">
                            <input id="title" name="title" value="{{ old('title') }}" type="text" class="form-control" placeholder="Title" autofocus="">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-xs-4 {{ $errors->has('date') ? ' has-error' : '' }}">
                            <input id="datepicker" name="date" value="{{ old('date') }}" placeholder="Publish Date" type="text" class="form-control">

                            @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <!-- Author and Tag  -->
                    <div class="form-group">
                      <div class="col-xs-4 {{ $errors->has('author') ? ' has-error' : '' }}">
                        <input id="author" name="author" value="{{ old('author') }}" type="text" class="form-control" placeholder="Author" autofocus="">

                        @if ($errors->has('author'))
                          <span class="help-block">
                            <strong>{{ $errors->first('author') }}</strong>
                          </span>
                        @endif
                      </div>

                      <div class="col-xs-4 {{ $errors->has('locale') ? ' has-error' : '' }}">
                        <select class="form-control" name="locale" placeholder="locale" >
                            @foreach (Config::get('languages') as $lang => $language)
                              <option value="{{ $lang }}">{{$language}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('locale'))
                          <span class="help-block">
                            <strong>{{ $errors->first('locale') }}</strong>
                          </span>
                        @endif
                      </div>


                    </div>

                    <!-- Content -->
                    <div class="{{ $errors->has('content') ? ' has-error' : '' }}">
                        <textarea name="content" form="form" placeholder="Article Detail">{{ old('content') }}</textarea>

                        @if ($errors->has('content'))
                            <span class="help-block">
                                <strong>{{ $errors->first('content') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Image Preview -->
                    <div class="form-group img-width {{ $errors->has('file') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <img id="image_preview" src="" alt="">

                            @if ($errors->has('file'))
                              <span class="help-block">
                                <strong>{{ $errors->first('file') }}</strong>
                              </span>
                            @endif
                        </div>
                    </div>

                    <!-- Remove Uploaded Image -->
                    <div class="form-group">
                        <div class="col-xs-4 color-li-primary trash ">
                            <div id="cancel_upload">
                                <!-- Bin -->
                                <span class="fa-stack icon">
                                  <i class="fa fa-circle fa-stack-2x"></i>
                                  <i class="fa fa-trash-o   fa-inverse fa-stack-1x"></i>
                                </span>

                                <!-- Image Size -->
                                <span id="image_name"></span>
                            </div>
                        </div>
                    </div>

                    <!-- Image Upload -->
                    <div class="form-group">
                        <div class="col-xs-12 upload-img">
                            <h4>Upload Image</h4>
                        </div>
                    </div>

                    <!-- Browse Button -->
                    <div class="form-group">
                        <div class="col-xs-4 browse-btn ">
                            <label for="uploaded_file">Browse</label>
                            <input type="file" name="file" id="uploaded_file" class="hidden" />


                        </div>
                    </div>

                    <!-- Publish Status -->
                    <!-- TODO: Turn into toggle -->
                    <div class="form-group">
                        <div class="col-xs-4  green-tick color-li-green">
                            <a href="{{ url('#') }}">
                                <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check  fa-inverse fa-stack-1x"></i>
                              </span>
                            </a>
                        </div>
                    </div>

                    <!-- Submit Buttons -->
                    <div class="form-group">
                        <div class="col-xs-8">

                            <!-- Apply -->
                            <!-- TODO: ADD PREVIEW -->
                            <button type="submit" class="btn btn-primary">
                              Apply and preview
                            </button>

                            <!-- Submit  -->
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!-- Section Content -->

    </div>
    <!-- #content -->

</div>
<!--row finish-->
@endsection
