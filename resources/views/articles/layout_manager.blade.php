@extends('layouts.app')

@section('content')

<div class = "row ">
    
    <div class = "col-xs-3 left-block" >
           <div class = "row"> 
        <div class = "col-xs-12 block">
            
            <ul>
            <li><a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-database fa-stack-1x"></i>
                </span>
               cms</a> 
            </li>
                <li>
                <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-folder-open fa-stack-1x"></i>
                </span>
                    content</a>
            
                
                
                </li>
                <li>
               <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-file fa-stack-1x"></i>
                </span>
                layout
                    </a>
                
                </li>
                </ul>
            </div></div>
        
        </div>
    
     
    
<div class="container col-xs-8" id="content">
    <div class="row section-header">
        <div class="col-xs-12">
            <h1>Layout - Menu Manager</h1>
            <div><hr class="line"></div>
        </div>
    </div>

    <div class="row section-content">
      <div class="col-xs-12">

        <h3>Add New Menu Link: </h3>
      <a href="{{ url('/articles/add') }}" class="btn btn-primary">New Link</a>
          
         
          
        

        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Pages</th>
                <th>Active</th>
                <th>URL Key</th>
                <th>ID</th>
                  <th>Move</th>
                <th>Edit/Delete</th>
              </tr>
            </thead>
            <tbody>

              
                  <tr>
                    <td>1.Home</td>
                    <td><ul><li class ="color-li-green"><a href="{{ url('#') }}"><span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check  fa-inverse fa-stack-1x"></i>
                        </span></a></li></ul></td>
                    <td>Home</td>
                       <td>01</td>
                        <td> <ul>

                        <!-- Edit -->
                       

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-arrow-down fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul></td>
                    {{-- Edit/Delete links --}}
                    <td>
                      <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul>

                    </td>

                  </tr>
                
                <tr>
                    <td>2.About us</td>
                    <td><ul><li class ="color-li-green"><a href="{{ url('#') }}"><span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check  fa-inverse fa-stack-1x"></i>
                        </span></a></li></ul></td>
                    <td>Home</td>
                       <td>01</td>
                        <td> <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-arrow-up fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-arrow-down fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul></td>
                    {{-- Edit/Delete links --}}
                    <td>
                      <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul>

                    </td>

                  </tr>
                
          
            </tbody>
          </table>

      </div>
    </div>
    </div>

    </div>

@endsection
