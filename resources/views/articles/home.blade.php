@extends('layouts.app')

@section('content')
<div class="row">

    @include('partials.side_nav')

    <div class="container col-xs-8" id="content">
        <div class="row section-header">
            <div class="col-xs-12">
                <h1>Layout - Site Admin - Press Articles</h1>
                <div><hr class="line"></div>
            </div>
        </div>

        <div class="row section-content">
          <div class="col-xs-12">

            <h3>Create New Article</h3>

            <a href="{{ url('/articles/create') }}" class="btn btn-primary">New Article</a>

            <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Date Published</th>
                    <th>Edit/Delete</th>
                  </tr>
                </thead>
                <tbody>

                  {{-- List of all the articles  --}}
                  @foreach ($articles as $article)
                      <tr>
                        <td>{{ $article->title }}</td>
                        <td>{{ $article->author }}</td>
                        <td>{{ date_format ( date_create($article->publish_date), 'd-m-Y') }}</td>

                        {{-- Edit/Delete links --}}
                        <td>
                          <ul>

                            <!-- Edit -->
                            <li>
                              <a href="{{ url('articles/edit/' . $article->id . '/' . App::getLocale() ) }}">
                                <span class="fa-stack icon">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                                </span>
                              </a>
                            </li>

                            <!-- Delete -->
                            <li>
                              <a href="#" onClick="return submitForm('{{ url('articles/delete/' . $article->id) }}');" >
                                <span class="fa-stack icon">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                                </span>
                              </a>
                            </li>

                          </ul>

                        </td>

                      </tr>
                  @endforeach
                </tbody>
              </table>

              <!-- Delete Article Form -->
              <form id="delete-form" action="" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
              <script>
                  function submitForm(href) {
                      event.preventDefault();
                      if(confirm('Are you sure you want to delete this article')) {
                        $("#delete-form").attr("action", href).submit();
                      }
                  }
              </script>
          </div>
        </div>
    </div>

</div>
@endsection
