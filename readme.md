# Wuhan Xingye Backend CMS

## Pre-Setup
Since the repository is built using the [Laravel Framework](http://laravel.com/docs), the following languages and extensions are required:
+ PHP
+ MySQL
+ Composer

This repository also makes use of [Laravel Elixir](https://laravel.com/docs/5.3/elixir) which requires the following:
+ Node.js
+ NPM
+ Gulp

The easiest way to install laravel on Windows is to install [wamp](https://sourceforge.net/projects/wampserver/) followed by [composer](https://getcomposer.org/download/) using the .exe file. Afterwards, install laravel using the following composer command:  
`composer global require "laravel/installer"`

## Laravel Setup
**Step One:**  
Clone the repository and cd into the directory

**Step Two:**  
Run the following command (inside the repositories directory) to install the needed dependencies:  
`composer install`

**Step Three:**  
Copy the .env.example file to .env and change the database related lines shown below to match your database  
```
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
**NOTE:** An easy way to setup a database and user on wamp is to log into phpmyadmin; then create a new user and database at the same time

**Step Four**  
Run the following commands to setup the database and run the application
```
php artisan key:generate
php artisan migrate
php artisan serve
```

**Extras:**  
In order to view the various webroots of the backend cms, you must first create a new user and then login  
An easy way to fill the database with data for testing purposes is to seed it using the following command:  
```
php artisan db:seed
```

## Laravel Elixir Setup
**Step One:**  
Install Node.js and NPM for your system using the relevant download found [here](https://nodejs.org/en/download/). This download should install both node.js and npm together. A simple way to test this is to run both of these commands:  
```
node -v
npm -v
```

**Step Two:**  
Install Gulp using the following command:
```
npm install --global gulp-cli
```

**Step Three**  
Run the following command from inside the directory that the repository was cloned into:  
```
npm install
```

## Basic Overview
#### Routing
This is handled by the __*routes/web.php*__ file. This file mostly contains lines with the following format:  
```
Route::get('route_name', ControllerName:function)
```

**route_name** - Refers to the name of the url you want added to the website e.g. __*'test'*__ would form the url __*/test*__  
**ControllerName** - Refers to the name of the controller found under the __*app/Http/Controllers*__ directory  
**function** - The name of the function called inside the __*ControllerName*__ file/class  

#### view function
Inside of the controllers mentioned in the routing section above; they often return a view. An example can be seen below:  
```
  return view('blade_file')
```

**blade_file** - The name of the file found inside the __*resources/views*__ directory. For example, returning __*'test'*__ would return the __*test.blade.php*__ file inside that directory.  

To return files inside subdirectories, dots are used to denote the subdirectory. E.g. __*'articles.add'*__ would return the __*app.blade.php*__ file inside the __*resources/views/articles*__ directory.

#### Blade Files
This second will only provide a quick introduction to how blade is used in this system. For a more thorough explanation of blade, click [here](https://laravel.com/docs/5.3/blade)

In this instance, almost every blade file will extend the __*'app.blade.php'*__ layout found under the  __*resources/views/layouts*__ directory. This template is basically the navbar, footer and a content tag used to denote where the content for each view should go. An example can be found below:

```php
@extends('layouts.app')

@section('content')

<!-- HTML GOES HERE -->

@endsection
```

#### SASS
This project is using [Laravel Elixir](https://laravel.com/docs/5.3/elixir) to compile the sass files that this project uses. All of these sass files can be found inside the __*resources/assets/sass*__ directory. The sass file that will most likely be edited is the __*_variables.scss*__ file as the other file is used to import the various stylings used throughout the project like bootstrap for example.

###### Compiling SASS
Running the `gulp watch` command from inside the repositories directory will look for any asset changes and re-compile them; i.e. re-compile changes to the sass files

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
