<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class SubmenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all the articles.
     *
     * @return \Illuminate\Http\Response
     */
    
   public function faq()
    {
        return view('submenu.faq'); // CHANGE NAME BASED ON FILE NAME INSIDE SUBMENU FOLDER
    }
    
     public function settings()
    {
        return view('submenu.settings'); // CHANGE NAME BASED ON FILE NAME INSIDE SUBMENU FOLDER
    }
    
    

}
