<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class ContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all the articles.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function file_manager()
    {
        return view('content.file_manager'); // CHANGE NAME BASED ON FILE NAME INSIDE CONTENT FOLDER
    }
    
     public function image_manager()
    {
        return view('content.image_manager'); // CHANGE NAME BASED ON FILE NAME INSIDE CONTENT FOLDER
    }
      
    
    public function image_manager_edit()
    {
        return view('content.image_manager_edit'); // CHANGE NAME BASED ON FILE NAME INSIDE CONTENT FOLDER
    }
    
    public function edit_file_manager()
    {
        return view('content.edit_file_manager'); // CHANGE NAME BASED ON FILE NAME INSIDE CONTENT FOLDER
    }
    
    

}
