<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{

    //
    public function switchLang($lang) {

      // Set the locale of the session to the language chosen if it is supported
      if(array_key_exists($lang, Config::get('languages') )  ){
        Session::set('applocale', $lang);
      }

      // Redirect back to the pervious page
      return Redirect::back();
    }

}
