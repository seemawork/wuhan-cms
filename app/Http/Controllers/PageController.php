<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

        
    public function pages()
    {
        return view('page.pages'); // CHANGE NAME BASED ON FILE NAME INSIDE PAGES FOLDER
    }
    
     public function newpage()
    {
        return view('page.newpage'); // CHANGE NAME BASED ON FILE NAME INSIDE PAGES FOLDER
    }

}
