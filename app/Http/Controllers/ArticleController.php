<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Auth;
use Validator;
use Illuminate\Http\Request;
use Config;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Show all the articles
    public function index()
    {
        $articles = Article::all();
      //  $articles = Article::where('company_id', Auth::user()->company_id)->get();
        return view('articles.home')->with('articles', $articles);
    }

    // Show the create article page
    public function viewCreate()
    {
        return view('articles.create');
    }

    // Handles POST request to CREATE an article
    public function create(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|alpha_spaces|max:255',
          'date' => 'required|date',
          'author' => 'required|alpha_spaces|max:100',
          'locale' => 'required|supported_locale',
          'content' => 'required',
          'file' => 'image|max:1000'
        ]);

        // Create article
        $article = new Article();
        $article->online = true;
        $article->author = $request->input('author');
        $article->publish_date = date_create($request->input('date'));
        $article->company_id = Auth::user()->company_id;
        $article->save();

        // Create the translated versions for the article
        $chosen_locale = $request->input('locale');
        foreach (Config::get('languages') as $locale => $language) {
          $title = ($chosen_locale === $locale) ? $request->input('title') : $language . " version: " . $request->input('title');
          $content = ($chosen_locale === $locale) ? $request->input('content') : $language . " version missing";

          $article->translateOrNew($locale)->title = $title;
          $article->translateOrNew($locale)->content = $content;
        }

        $article->save();
        return redirect('articles');
    }

    // Handles POST request to DELETE an article
    public function delete(Request $request, $id)
    {
      $article = Article::find($id);

      // Delete the article if it exists
      if($article) {
          $article->deleteTranslations();
          $article->delete();
      }

      return redirect('articles');
    }

    // View the edit page for the selected article in the selected language
    public function viewEdit($id, $lang)
    {
        $article = Article::find($id);
        if($article && $article->hasTranslation($lang)  ) {
            $result =  $article->translate($lang);
            $result->publish_date = $article->publish_date;
            $result->author = $article->author;
            $result->online = $article->online;
            $result->company_id = $article->company_id;

            return view('articles.edit')->with('article', $result);
        }

        return redirect('articles');
    }

    // Handles POST request to EDIT an article
    public function edit(Request $request, $id, $lang)
    {
        $article = Article::find($id);
        if($article && $article->hasTranslation($lang)  ) {
            $translation =  $article->translate($lang);

            // Validate Inputs
            $this->validate($request, [
              'title' => 'alpha_spaces|max:255',
              'date' => 'date',
              'author' => 'alpha_spaces|max:100',
              'file' => 'image|max:1000'
            ]);

            $article->publish_date = ($request->has('date')) ? date_create($request->input('date')) : $article->publish_date;
            $article->author = ($request->has('author') && strlen($request->input('author')) > 0) ? $request->input('author') : $article->author;

            $translation->title = ($request->has('title') && strlen($request->input('title')) > 0) ? $request->input('title') : $translation->title;
            $translation->content = ($request->has('content') && strlen($request->input('content')) > 0) ? $request->input('content') : $translation->content;

            $article->save();
            $translation->save();
            return redirect('articles');
        }

        return redirect('articles');
    }

    public function layout_manager()
    {
        return view('articles.layout_manager');
    }

    public function layout_newlink()
    {
        return view('articles.layout_newlink');
    }
}
