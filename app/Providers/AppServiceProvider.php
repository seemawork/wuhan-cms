<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        // Validates alpha numeric characters with spaces
        Validator::extend('alpha_spaces', function ($attribute, $value) {
          return preg_match('/^[0-9\pL\s]+$/u', $value);
        });

        // Checks to see if the locale selected is in the applications list os supported locales
        Validator::extend('supported_locale', function ($attribute, $value) {
          return array_key_exists($value, Config::get('languages'));
        });
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}
