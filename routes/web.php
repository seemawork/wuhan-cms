<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route that handles redirection based on whether the user is logged in or not
Route::get('/', function () {
    if(Auth::guest() ) {
        return redirect('login');
    } else {
        return redirect('home');
    }
});

// Route that handles locale switching
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

// Routes that handle login, register and the dashboard
Auth::routes();
Route::get('home', 'HomeController@index');

// Routes that handle articles
Route::get('articles', 'ArticleController@index');
Route::get('articles/create', 'ArticleController@viewCreate');
Route::post('articles/create', 'ArticleController@create');
Route::get('articles/edit/{id}/{lang}', 'ArticleController@viewEdit');
Route::post('articles/edit/{id}/{lang}', 'ArticleController@edit');
Route::post('articles/delete/{id}', 'ArticleController@delete');

Route::get('layout', 'ArticleController@layout_manager');
Route::get('layoutnewlink', 'ArticleController@layout_newlink');

//page controller web route

Route::get('pages/newpage', 'PageController@newpage');
Route::get('pages', 'PageController@pages');

//content controller web route
Route::get('editimage', 'ContentController@image_manager_edit');
Route::get('editfile', 'ContentController@edit_file_manager');
Route::get('file', 'ContentController@file_manager');
Route::get('image', 'ContentController@image_manager');

//submenu controller web route
Route::get('faq', 'SubmenuController@faq');
Route::get('settings', 'SubmenuController@settings');

// TEST WEB ROUTE
#Route::get('test', 'ArticleController@test');
