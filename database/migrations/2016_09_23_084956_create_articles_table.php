<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Articles Table
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');

            // The person who wrote of the article
            $table->string('author');

            // Is the article viewable or not
            $table->boolean('online');

            // publsh date
            $table->datetime('publish_date');

            // Set reference to the company to article belongs to
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->timestamps();
        });

        /*
         * Articles Translation Tables
         * Contains all the content that would need to be translated
        */
        Schema::create('article_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();

            // The language for the article
            $table->string('locale')->index();

            // Article content that may need translating
            $table->string('title');
            $table->text('content');

            // Set the reference to the article the translation belongs to
            $table->unique(['article_id','locale']);
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
