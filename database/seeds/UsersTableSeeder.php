<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Remove all the users inside the table
        User::truncate();

        // Create the users
        $users = [
            ['name' => 'user', 'email' => 'user@test.test', 'password' => bcrypt('testing'), 'company_id' => 1 ],
            ['name' => 'admin', 'email' => 'admin@test.test', 'password' => bcrypt('testing'), 'company_id' => 1 ],
            ['name' => 'superadmin', 'email' => 'super@test.test', 'password' => bcrypt('testing'), 'company_id' => 1 ]
        ];

        // Add them to the database
        User::insert($users);
    }
}
