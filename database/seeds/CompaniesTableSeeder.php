<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Remove Existing Data
        Company::truncate();

        // Defines the various companies
        $companies = [
            ['name' => 'Certified Public Accountants' ],
            ['name' => 'Engineering Consulting' ],
            ['name' => 'Assets Appraisal' ],
            ['name' => 'Tender Agency' ],
            ['name' => 'Tax Affairs' ]
        ];

        // Add them to the database
        Company::insert($companies);
    }
}
