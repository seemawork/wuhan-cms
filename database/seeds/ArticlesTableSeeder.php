<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Article;
use App\Company;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Remove Existing Data
        Article::truncate();

        // Get all the companies inside the CMS
        $companies = Company::all();

        foreach($companies as $company) {
          // Create the article
          $article = new Article(); //::create();
          $article->author = "test";
          $article->online = 1;
          $article->publish_date= new DateTime();
          $article->company_id = $company->id;
          $article->save();

          // Create the article translations
          foreach (['en', 'cn'] as $locale) {
            $article->translateOrNew($locale)->title = "{$company->name}: {$locale}";
            $article->translateOrNew($locale)->content = "PLACEHOLDER CONTENT: {$locale}";
          }

          $article->save();
        }



    }
}
