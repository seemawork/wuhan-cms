 <div class = "col-xs-3 left-block" >
           <div class = "row"> 
        <div class = "col-xs-12 block">
            
            <ul>
            <li><a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-database fa-stack-1x"></i>
                </span>
               cms</a> 
            </li>
                <li>
                <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-folder-open fa-stack-1x"></i>
                </span>
                    content</a>
            
                
                
                </li>
                <li>
               <a href= "">
                <span class="fa-stack icon">
                    <i class="fa fa-circle fa-inverse fa-stack-2x"></i>
                    <i class="fa fa-file fa-stack-1x"></i>
                </span>
                layout
                    </a>
                
                </li>
                </ul>
            </div></div>
        
        </div>
        
        
        
        
        @extends('layouts.app')
        
        
        
        
        
        
        

@section('content')
<div class="container" id="content">
    <div class ="row">
       
        <div class = "col-xs-9 ">
    <div class="row section-header">
       <h1>File Manager</h1>
   
<div><hr class = "line"></div>
            <h3>Upload File</h3>
            
          </div>   
    <div class="row section-content">
      

        

        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Files</th>
                <th>Active</th>
                <th>Page</th>
                <th>Edit/Delete</th>
              </tr>
            </thead>
            <tbody>

              {{-- List of all the articles  --}}
             
                  <tr>
                    <td>xyz</td>
                    <td><a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check fa-inverse fa-stack-1x"></i>
                            </span>
                          </a></td>
                    <td>12/11/16</td>

                    {{-- Edit/Delete links --}}
                    <td>
                      <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul>

                    </td>

                  </tr>
                <tr>
                    <td>xyz</td>
                    <td><a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check fa-inverse fa-stack-1x"></i>
                            </span>
                          </a></td>
                    <td>12/11/16</td>

                    {{-- Edit/Delete links --}}
                    <td>
                      <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                              </span>
                          </a>
                        </li>

                      </ul>

                    </td>

                  </tr>
                <tr>
                    <td>xyz</td>
                    <td><a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-check fa-inverse fa-stack-1x"></i>
                            </span>
                          </a></td>
                    <td>12/11/16</td>

                    {{-- Edit/Delete links --}}
                    <td>
                      <ul>

                        <!-- Edit -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                        <!-- Delete -->
                        <li>
                          <a href="{{ url('#') }}">
                            <span class="fa-stack icon">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-trash fa-inverse fa-stack-1x"></i>
                            </span>
                          </a>
                        </li>

                      </ul>

                    </td>

                  </tr>
                
            
            </tbody>
          </table>

   
    </div>
        </div>
    </div>
</div>
@endsection
